


#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/robot_state/conversions.h>
// // // // // // /*
// // // // // // #include <moveit_msgs/DisplayRobotState.h>
// // // // // // #include <moveit_msgs/DisplayTrajectory.h>
// // // // // // 
// // // // // // #include <moveit_msgs/AttachedCollisionObject.h>
// // // // // // #include <moveit_msgs/CollisionObject.h>
// // // // // // 
// // // // // // #include <moveit_visual_tools/moveit_visual_tools.h>
// // // // // // 
// // // // // // #include "eureca_assembly/moveIiwa.h"
// // // // // // 
// // // // // // #include <string> 
// // // // // // #include <iostream>
// // // // // // #include <algorithm>
// // // // // // #include <cstdlib> 
// // // // // // #include <ctime>
// // // // // // #include "geometric_shapes/shape_operations.h"
// // // // // // 
// // // // // // #include "std_msgs/String.h"
// // // // // // #include "std_msgs/Float64MultiArray.h"*/


using namespace std;

const string PLANNING_GROUP = "manipulator";
const string endEffector    = "Gripper_ee_link";
const double pi = 3.1415926535;

#if 0
class SidePanSrv
{
public:
  
  std::vector<double> jointHome;
  std::vector<double> rotationAttachPose;
  std::vector<double> robotOrigin;
  std::vector<double> preAttachPoint;
  std::vector<double> preAttachPointRot;
  std::vector<double> gripPoint;
  std::vector<double> secondPanel;
  std::vector<double> panelLean;
  std::vector<double> intermediatePose;
  std::vector<double> rotationGrippingPose;
  std::vector<double> cabPose;
  std::vector<double> cabTrasl;
  std::vector<double> sideCab;
  std::vector<double> panelToCartRot;
  std::vector<double> panelToCartTrasl;
  tf::Transform worldToRobot;
  tf::Transform panelToCartOriginTrasl;
  tf::Transform panelToCartOriginRot;
  tf::Transform CabinaToPreAttach;
  tf::Transform CabinaToPreAttachRot;
  tf::Transform gripPointToPanelOrigin;
  tf::Transform secondPanelOrigin;
  tf::Transform panelLeanGrip;
  tf::Transform gripperRotation;
  tf::Transform attachPointToCabinaOrigin;
  tf::Transform traslAttachPointToCabinaOrigin;
  tf::Transform sideCabinaOrigin;
  tf::Transform endEffectorPose4Rotation;
  tf::Transform middlePose;
  tf::Transform intermediate ;
  tf::Vector3 originInt;
  tf::Quaternion rotationInt;

  std::vector<double> grippingPose;
  std::vector<double> cabinaAttachPose;
  
  moveit::planning_interface::MoveGroupInterface* move_group;
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

  geometry_msgs::Pose getpose(tf::Vector3 xyz, tf::Quaternion wxyz)
  {
      
    geometry_msgs::Pose targetPose;
    
    targetPose.position.x = xyz.getX();
    targetPose.position.y = xyz.getY();
    targetPose.position.z = xyz.getZ();
    
    targetPose.orientation.x = wxyz.getX();
    targetPose.orientation.y = wxyz.getY();
    targetPose.orientation.z = wxyz.getZ();
    targetPose.orientation.w = wxyz.getW();
    
    return targetPose;
  }
  void printPoseWarn(tf::Vector3 origin, tf::Quaternion rotation){
    
    ROS_WARN("position x = %f", origin.getX());
    ROS_WARN("position y = %f", origin.getY());
    ROS_WARN("position z = %f", origin.getZ());
    
    ROS_WARN("rotation w= %f", rotation.getW());
    ROS_WARN("rotation x= %f", rotation.getX());
    ROS_WARN("rotation y= %f", rotation.getY());
    ROS_WARN("rotation z= %f", rotation.getZ());
  }
  void printPoseWarn(tf::Transform pose){
    
    ROS_WARN("position x = %f", pose.getOrigin().getX());
    ROS_WARN("position y = %f", pose.getOrigin().getY());
    ROS_WARN("position z = %f", pose.getOrigin().getZ());
    
    ROS_WARN("rotation w= %f", pose.getRotation().getW());
    ROS_WARN("rotation x= %f", pose.getRotation().getX());
    ROS_WARN("rotation y= %f", pose.getRotation().getY());
    ROS_WARN("rotation z= %f", pose.getRotation().getZ());
  }

  void poseDefinition(std::vector<double> *pose ,tf::Transform *transform){
    
    tf::Vector3 v = tf::Vector3(pose->at(0),pose->at(1),pose->at(2));
    tf::Quaternion q;
      
    if(pose->size() == 6)
      q = tf::createQuaternionFromRPY(pose->at(3),pose->at(4),pose->at(5)); 
    else
      q = tf::Quaternion(pose->at(4),pose->at(5),pose->at(6),pose->at(3)); 
    
    transform->setOrigin(v);
    transform->setRotation(q);	
  }

  void addObject(string collisionObjID, string planningFrame, 
      tf::Transform *pose, moveit_msgs::CollisionObject *collision_object, 
      Eigen::Vector3d b={1.0, 1.0, 1.0},
      string stlID = ""){

    if (stlID == "")
      stlID = collisionObjID;

    collision_object->id = collisionObjID;
    shapes::Mesh* m = shapes::createMeshFromResource("package://sim_iiwa_description/stl/"+stlID, b);

    shape_msgs::Mesh mesh;
    shapes::ShapeMsg mesh_msg;
    shapes::constructMsgFromShape(m, mesh_msg);
    mesh = boost::get<shape_msgs::Mesh>(mesh_msg);
    
    collision_object->meshes.resize(1);
    collision_object->mesh_poses.resize(1);
    collision_object->meshes[0] = mesh;
    collision_object->header.frame_id = planningFrame;
    
    tf::Vector3 origin = pose->getOrigin();
    tf::Quaternion rotation = pose->getRotation();
    
    geometry_msgs::Pose Pose;
    Pose = getpose(origin,rotation);
    
    collision_object->mesh_poses[0] = Pose;
      
    collision_object->meshes.push_back(mesh);
    collision_object->mesh_poses.push_back(collision_object->mesh_poses[0]);
    collision_object->operation = collision_object->ADD;
  }


  bool planAndMove(geometry_msgs::Pose targetPose, const string endEffector){

    ROS_INFO("-- Set Target");  
    move_group->setPoseTarget(targetPose,endEffector);
    
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    if(!(move_group->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS))
      return false;
    
    ROS_INFO("-- Moveit Plan"); 
    move_group->execute(my_plan);
    
    return true;
  }
  
  bool planAndMove(std::vector<double> jointTargetPose){

    ROS_INFO("-- Set Target");  
    move_group->setJointValueTarget(jointTargetPose);

    ROS_INFO("-- Moveit Plan");  
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    if(!(move_group->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS))
      return false;
    
    move_group->execute(my_plan);
    
    return true;
  }

  void noSuccess(moveit_msgs::CollisionObject *collision_object,
      moveit::planning_interface::PlanningSceneInterface *planning_scene_interface,
      std::vector<std::string> *object_ids)
  {
    ROS_ERROR("pianificazione non riuscita");
    move_group->detachObject(collision_object->id);
    ros::Duration(1.0).sleep();
    planning_scene_interface->removeCollisionObjects(*object_ids);
  }

  double roll,pitch,yaw;

  void toEulerAngle(tf::Quaternion q, double *roll, double *pitch, double *yaw)
  {
    // roll (x-axis rotation)
    double sinr = 2 * (q.getW() * q.getX() + q.getY() * q.getZ());
    double cosr = 1 - 2 * (q.getX() * q.getX() + q.getY() * q.getY());
    *roll = atan2(sinr, cosr);
    // pitch (y-axis rotation)
    double sinp = 2 * (q.getW() * q.getY() - q.getZ() * q.getX());
    if (fabs(sinp) >= 1)
      *pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
    else
      *pitch = asin(sinp);
    // yaw (z-axis rotation)
    double siny = 2 * (q.getW() * q.getZ() + q.getX() * q.getY());
    double cosy = 1 - 2 * (q.getY() * q.getY() + q.getZ() * q.getZ());  
    *yaw = atan2(siny, cosy);
  }



  


  SidePanSrv( ros::NodeHandle& nh, moveit::planning_interface::MoveGroupInterface* mgroup ) : move_group( mgroup )
  {
    if(!nh.getParam("/new_sim_fixed_positions/jointHome", jointHome))
    { ROS_ERROR("posizione jointHome non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/rotationAttachPose", rotationAttachPose))
    { ROS_ERROR("posizione rotationAttachPose non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/robotOrigin", robotOrigin))
    { ROS_ERROR("posizione origine non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/preAttachPoint", preAttachPoint))
    { ROS_ERROR("posizione preAttachPoint non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/preAttachPointRot", preAttachPointRot))
    { ROS_ERROR("posizione preAttachPointRot non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/gripPoint", gripPoint))
    { ROS_ERROR("posizione gripPoint non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/secondPanel", secondPanel))
    { ROS_ERROR("posizione secondPanel non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/panelLean", panelLean))
    { ROS_ERROR("posizione panelLean non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/intermediatePose", intermediatePose))
    { ROS_ERROR("posizione intermediatePose non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/rotationGrippingPose", rotationGrippingPose))
    { ROS_ERROR("posizione rotationGrippingPose non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/cabPose", cabPose))
    { ROS_ERROR("posizione cabPose non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/cabTrasl", cabTrasl))
    { ROS_ERROR("posizione cabTrasl non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/sideCab", sideCab))
    { ROS_ERROR("posizione sideCab non caricata");
      return;
    }
    if(!nh.getParam("/new_sim_fixed_positions/panelToCartRot", panelToCartRot))
    { ROS_ERROR("posizione panelToCartRot non caricata");
    }
    if(!nh.getParam("/new_sim_fixed_positions/panelToCartTrasl", panelToCartTrasl))
    { ROS_ERROR("posizione panelToCartTrasl non caricata");
    }
    
  //--------------------------------------key poses definition----------------------------------------------------

  // trasformazione da origine del mondo a origine del robot

    int posizione = 0;
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    
    poseDefinition(&robotOrigin,&worldToRobot);

    cout<<"posizione:"+to_string(posizione++)+"\n";
    
    poseDefinition(&panelToCartTrasl,&panelToCartOriginTrasl);
    poseDefinition(&panelToCartRot,&panelToCartOriginRot);
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    // trasformazione da origine della cabina la punto di aggancio pannello
    poseDefinition(&preAttachPoint,&CabinaToPreAttach);
    poseDefinition(&preAttachPointRot,&CabinaToPreAttachRot);
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    //  trasformazione da punto di afferraggio pannello a origine del pannello
    
    poseDefinition(&gripPoint,&gripPointToPanelOrigin);
    poseDefinition(&secondPanel,&secondPanelOrigin);
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    poseDefinition(&panelLean,&panelLeanGrip);
    
  //  rotazione del gripper
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    poseDefinition(&rotationGrippingPose,&gripperRotation);
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    // posizione dell' origine della cabina 
    
    poseDefinition(&cabPose,&attachPointToCabinaOrigin);
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    poseDefinition(&cabTrasl,&traslAttachPointToCabinaOrigin);
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    
    poseDefinition(&sideCab,&sideCabinaOrigin);
    
    // posa del robot all aggancio del pannello
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    
    poseDefinition(&rotationAttachPose,&endEffectorPose4Rotation);
    
  //   intermezzo sopra alla capaattachPointToCabinaOrigin
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    poseDefinition(&intermediatePose,&middlePose);
    intermediate = worldToRobot * middlePose;
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    originInt = intermediate.getOrigin();
    rotationInt = intermediate.getRotation();
    
    cout<<"posizione:"+to_string(posizione++)+"\n";
    
  }


  bool plan_and_move(eureca_assembly::moveIiwa::Request  &req,
            eureca_assembly::moveIiwa::Response &res)
  {
    
    int poseCount;
    bool success;
    for (int i = 0; i<req.grippingPose.size();i++)
      grippingPose.push_back(req.grippingPose[i]);
    for (int i = 0; i<req.cabinaPose.size();i++)
      cabinaAttachPose.push_back(req.cabinaPose[i]);

    
    geometry_msgs::PoseStamped currentPose;
    geometry_msgs::PoseStamped orient;
    
    move_group->setPlanningTime(15.0);

  //   currentPose = move_group->getCurrentPose(endEffector);

  //----------------------------------------------inizio a muovere-------------------------------------

    ROS_INFO("Plan and Move!");
    move_group->setStartStateToCurrentState();
    planAndMove( jointHome);

    geometry_msgs::Pose targetPose;
    
  //   success = true;
  //   
  // //-------------------------------aspetto pubblicazione posizione pannello---------------------------------------------- 
  // 
  //   tf::Transform endEffectorGrippingPose;
  //   poseDefinition(&grippingPose,&endEffectorGrippingPose);
  //   
  //   tf::Transform gripPose;
  //   gripPose = worldToRobot * endEffectorGrippingPose * gripperRotation;
  // 
  // //-------------------------------add panel to planning scene---------------------------------------------- 
  //   
  //   string planningFrame = move_group->getPlanningFrame();
  //   
  //   string collisionObjID = "CALITO_sidePanel.stl";
  //   
  //   tf::Transform panelPose ;
  //   panelPose = worldToRobot * endEffectorGrippingPose * gripPointToPanelOrigin * panelLeanGrip ;
  // 	  
  //   moveit_msgs::CollisionObject collision_object_panel;
  //   Eigen::Vector3d scalePan={0.001, 0.001, 0.001};
  // 
  //   addObject(collisionObjID, planningFrame, &panelPose, &collision_object_panel,scalePan);
  // 
  // 
  // //-------------------------------add cart to planning scene---------------------------------------------- 
  //   
  //   collisionObjID = "carrelloKUKA.stl";
  //   
  //   tf::Transform cartPose ;
  //   cartPose = worldToRobot * endEffectorGrippingPose * gripPointToPanelOrigin * panelToCartOriginTrasl * panelToCartOriginRot;
  // 	  
  //   moveit_msgs::CollisionObject collision_object_cart;
  //   Eigen::Vector3d scaleCart={0.001, 0.001, 0.001};
  //   addObject(collisionObjID, planningFrame, &cartPose, &collision_object_cart,scaleCart);
  //   
  // 
  // //-------------------------------aspetto pubblicazione posizione cabina---------------------------------------------- 
  // 
  //   tf::Transform endEffectorAttachPose;
  //   poseDefinition(&cabinaAttachPose,&endEffectorAttachPose);
  //   
  //   tf::Transform robotPose3 = worldToRobot * endEffectorAttachPose * CabinaToPreAttachRot * CabinaToPreAttach;
  //   
  //   tf::Vector3 origin3 = robotPose3.getOrigin();
  //   tf::Quaternion rotation3 = robotPose3.getRotation();
  //   
  //   cout<<"pose 3 \n";
  //   printPoseWarn(robotPose3);
  //   
  // // 	 aggancio
  //   tf::Transform robotPose4 = worldToRobot * endEffectorAttachPose;
  //   
  //   tf::Vector3 origin4 = robotPose4.getOrigin();
  //   tf::Quaternion rotation4 = robotPose4.getRotation();
  //   
  //   cout<<"pose 4 \n";
  //   printPoseWarn(robotPose4);
  //     
  // //-------------------------------add cabina to planning scene---------------------------------------------- 
  //   
  //   moveit_msgs::CollisionObject collision_object_cabina;
  //   Eigen::Vector3d scaleFus={0.001, 0.001, 0.001};
  //   collisionObjID = "CALITO_newfus.stl";
  //   
  //   tf::Transform cabinaPose ;
  //   cabinaPose = worldToRobot * endEffectorAttachPose * attachPointToCabinaOrigin * traslAttachPointToCabinaOrigin;
  // 
  //   printPoseWarn(cabinaPose);
  //   
  //   addObject(collisionObjID, planningFrame, &cabinaPose, &collision_object_cabina, scaleFus);
  //   
  //   moveit_msgs::CollisionObject collision_object_sideCabina;
  //   collisionObjID = "CALITO_sidefus.stl";
  //   
  //   
  //   tf::Transform sideCabinaPose ;
  //   sideCabinaPose = worldToRobot * endEffectorAttachPose * attachPointToCabinaOrigin * traslAttachPointToCabinaOrigin * sideCabinaOrigin;
  // 
  //   printPoseWarn(sideCabinaPose);
  //   
  //   addObject(collisionObjID, planningFrame, &sideCabinaPose, &collision_object_sideCabina, scaleFus);
  //   
  // 
  // //  add objects to planning scene
  //   
  //   std::vector<moveit_msgs::CollisionObject> collision_vector;
  //   collision_vector.push_back(collision_object_panel);
  //   collision_vector.push_back(collision_object_cart);
  //   collision_vector.push_back(collision_object_cabina);
  //   collision_vector.push_back(collision_object_sideCabina);
  //     
  //   planning_scene_interface.addCollisionObjects(collision_vector);
  //   
  //   ros::Duration(1.0).sleep();
  //   
  //   std::vector<std::string> object_ids;
  //   object_ids.push_back(collision_object_panel.id);
  //   object_ids.push_back(collision_object_cart.id);
  //   object_ids.push_back(collision_object_cabina.id);
  //   object_ids.push_back(collision_object_sideCabina.id);
  // 
  // //---------------------------set position and orientation grip------------------------------------
  // 
  //   move_group->setStartStateToCurrentState();
  //   
  //   targetPose = getpose(gripPose.getOrigin(),gripPose.getRotation());
  //   
  //   if(!(planAndMove(&move_group, targetPose, endEffector))){
  //     noSuccess(&move_group,&collision_object_panel,&planning_scene_interface,&object_ids);
  //     planAndMove(&move_group, jointHome);
  //     
  //   }
  // 
  //   poseCount++;
  //   
  //   move_group->attachObject(collision_object_panel.id,endEffector);
  // 
  // //-------------------------------set position and orientation 3----------------------------------------
  //   
  //   move_group->setStartStateToCurrentState();
  //   
  //   targetPose = getpose(origin3,rotation3);
  //   
  //   if(!(planAndMove(&move_group, targetPose, endEffector))){
  //     noSuccess(&move_group,&collision_object_panel,&planning_scene_interface,&object_ids);
  //     planAndMove(&move_group, jointHome);
  //     return false;
  //   }
  //   
  //   poseCount++;
  //   
  // //-------------------------------Cartesian path--------------------------------
  // 						
  //   currentPose = move_group->getCurrentPose(endEffector);
  // 					      
  //   double deltaX = robotPose4.getOrigin().getX() - currentPose.pose.position.x;
  //   double deltaY = robotPose4.getOrigin().getY() - currentPose.pose.position.y;
  //   double deltaZ = robotPose4.getOrigin().getZ() - currentPose.pose.position.z;
  //   
  //   double deltaRw = robotPose4.getRotation().getW() - currentPose.pose.orientation.w;
  //   double deltaRx = robotPose4.getRotation().getX() - currentPose.pose.orientation.x;
  //   double deltaRy = robotPose4.getRotation().getY() - currentPose.pose.orientation.y;
  //   double deltaRz = robotPose4.getRotation().getZ() - currentPose.pose.orientation.z;
  //   
  //   std::vector<geometry_msgs::Pose> waypoints;
  //   geometry_msgs::Pose target_pose3 = currentPose.pose;
  //   
  // //   waypoints.push_back(target_pose3);
  //   target_pose3.position.x += deltaX;
  //   target_pose3.position.y += deltaY;
  //   target_pose3.position.z += deltaZ;
  //   
  //   target_pose3.orientation.w += deltaRw;
  //   target_pose3.orientation.x += deltaRx;
  //   target_pose3.orientation.y += deltaRy;
  //   target_pose3.orientation.z += deltaRz;
  //   
  //   waypoints.push_back(target_pose3);  // up
  //   
  //   moveit_msgs::RobotTrajectory cartesianTrajectory;
  //   const double jump_threshold = 0.0;
  //   const double eef_step = 0.01;
  //   double fraction = move_group->computeCartesianPath(waypoints, eef_step, jump_threshold, cartesianTrajectory);
  //   ROS_WARN("trajectory fraction %f",fraction);
  //   
  //   if(fraction < 0.95){
  //     noSuccess(&move_group,&collision_object_panel,&planning_scene_interface,&object_ids);
  //     cout << "traietoria non finita, posizione raggiungibile?";
  //     planAndMove(&move_group, jointHome);
  //   }
  //   
  //   moveit::planning_interface::MoveGroupInterface::Plan cartesian_plan;
  //   cartesian_plan.trajectory_ = cartesianTrajectory;
  //   move_group->execute(cartesian_plan);
  // 
  //   move_group->detachObject(collision_object_panel.id);
  //   ros::Duration(1.0).sleep();
  // 	
  // //-------------------------------return to home position----------------------------------------
  //   
  //   move_group->setStartStateToCurrentState();
  //   
  //   if(!(planAndMove(&move_group, jointHome))){
  //     noSuccess(&move_group,&collision_object_panel,&planning_scene_interface,&object_ids);
  //     planAndMove(&move_group, jointHome);
  //   }

    res.success = true;
    
    return true;
  }
};
#endif





int main(int argc, char **argv)
{
  ros::init(argc, argv, "plan_and_move_iiwa");
  ros::NodeHandle nh;
  
  ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
  
  ros::AsyncSpinner spinner(1);
  spinner.start();

  ROS_INFO("\n\n\n\n\t\t\tCreating the Move group...\n\n\n");
  moveit::planning_interface::MoveGroupInterface::Options opt( "manipulator", "/olivia/robot_description" );
  moveit::planning_interface::MoveGroupInterface  move_group( opt );  
  
  ROS_INFO("\n\n\n\t\t\tMove group Created!!!!\n\n\n");
  /*
  SidePanSrv sie_pan_srv( nh, &move_group );

  
  ros::ServiceServer service = nh.advertiseService("move_iiwa", &SidePanSrv::plan_and_move, &sie_pan_srv);
  */
  ros::waitForShutdown();

  return 0;
}

