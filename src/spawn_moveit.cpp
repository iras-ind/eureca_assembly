#include <ros/ros.h>
#include <ros/package.h>
#include <actionlib/server/action_server.h>

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <eureca_assembly/TriggerAction.h>
#include <geometry_msgs/Quaternion.h>

const std::string EURECA_MOVEIT_SPAWNER_DEFAULT_NS = "spawn_moveit";

class MoveitSpawner
{
protected:

  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<eureca_assembly::TriggerAction> as_; // NodeHandle instance must be created before this line. Otherwise strange error occurs.
  std::string action_name_;
 
  eureca_assembly::TriggerResult result_;
  
  static void toEulerAngle(const geometry_msgs::Quaternion& q, double& roll, double& pitch, double& yaw)
  {
    double sinr = +2.0 * (q.w * q.x + q.y * q.z);
    double cosr = +1.0 - 2.0 * (q.x * q.x + q.y * q.y);
    roll = atan2(sinr, cosr);

    double sinp = +2.0 * (q.w * q.y - q.z * q.x);
    if (fabs(sinp) >= 1)
	    pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
    else
	    pitch = asin(sinp);

    double siny = +2.0 * (q.w * q.z + q.x * q.y);
    double cosy = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);  
    yaw = atan2(siny, cosy);
  
  }

public:

  MoveitSpawner(std::string name) 
  : nh_("~")
  , as_(nh_, name, boost::bind(&MoveitSpawner::executeCB, this, _1), false)
  , action_name_(name)
  {
    as_.start();
  }

  ~MoveitSpawner(void)
  {
  }

  void executeCB(const eureca_assembly::TriggerGoalConstPtr &goal)
  {
    std::string path = ros::package::getPath("eureca_moveit");
    std::string robotName = goal->robot;
    
    geometry_msgs::Quaternion q;
    q.x = goal->origin.orientation.x;
    q.y = goal->origin.orientation.y;
    q.z = goal->origin.orientation.z;
    q.w = goal->origin.orientation.w;
    
    double r,p,y;
    
    toEulerAngle(q,r,p,y);
    
    std::string origin_xyz = std::to_string( goal->origin.position.x) + " " + std::to_string( goal->origin.position.y) + " " + std::to_string( goal->origin.position.z);
    std::string origin_rpy = std::to_string( r ) + " " + std::to_string( p ) +  " " + std::to_string( y ) ;
    
    int ret = system( ("roslaunch "+ path + "/launch/moveit_planning_execution.launch robot_name:="+ robotName +" origin_xyz:=\"'" +origin_xyz+"'\" origin_rpy:=\"'"+origin_rpy+"'\"").c_str() );
  
    if( ret > 0  )
    {
      result_.success= 1;
      ROS_INFO("%s: Succeeded", action_name_.c_str());
      as_.setSucceeded(result_);
    }
  }


};


int main(int argc, char** argv)
{
  ros::init(argc, argv, EURECA_MOVEIT_SPAWNER_DEFAULT_NS);

  MoveitSpawner fibonacci( EURECA_MOVEIT_SPAWNER_DEFAULT_NS);
  ros::spin();

  return 0;
}